package com.example.didicarrace;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;

/**
 * Created by Funny_One on 2017/11/4.
 */
//游戏运行程序
//代码规范人：20162317袁逸灏
public class LevelProcedure extends Activity implements DialogInterface.OnClickListener,View.OnClickListener,Runnable{
    //声明静态GameView，便于调用GameView的方法
    private static GameView view = null;
    //声明暂停按键
    private Button stopBtn;
    //==================背景音乐的播放=====================
    //接收器
    private Intent receiver;
    //歌曲id
    private int songID;
    //音乐播放器
    private MediaPlayer mp;
    //=====================================================
    //通关活动跳转Intent
    private Intent winIntent;
    //结束活动跳转Intent
    private Intent endIntent;
    //返回主页面的Intent
    private Intent backIntent;
    //用线程监听分数
    private Thread thread;




    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gameview);
        view = (GameView)findViewById(R.id.GameView);
        initUI();
        //线程开始执行
        thread.start();

    }

    //组件初始化
    private void initUI(){
        //暂停按键初始化
        stopBtn = (Button)findViewById(R.id.stopBtn);
        //暂停按键监听
        stopBtn.setOnClickListener(this);
        //接收器初始化
        receiver =getIntent();
        //通关活动跳转初始化
        winIntent = new Intent(this,Win.class);
        //结束活动Intent初始化
        endIntent = new Intent(this,End.class);
        //返回主页面活动初始化
        backIntent = new Intent(this,MainCover.class);
        //监听线程初始化
        thread = new Thread(this);
        //用接收器获取选择车辆的id，并传入自定义view中来选择画哪辆车
        view.carIDSetter(Integer.parseInt((receiver.getStringExtra("carID"))));

        //=========================音乐播放=============================
        //用接收器获取曲目id
        songID = receiver.getIntExtra("songID",0);
        //音乐播放器初始化
        mp = MediaPlayer.create(this, songID);
        //设置为单曲循环
        mp.setLooping(true);
        //开始播放
        mp.start();
        //==============================================================

    }





    //========================按返回键会出现弹窗==========================



    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //判断为按下返回键后
        if(keyCode==KeyEvent.KEYCODE_BACK){
            //游戏暂停运行
           GamePause();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        //按继续键的时候
        if(i == -3){
            //游戏继续运行
            view.start();
            //音乐继续播放
            mp.start();
        }
        //按退出键的时候
        else {
            //音乐停止播放
            mp.stop();
            startActivity(backIntent);
            //该活动被杀死
            android.os.Process.killProcess(android.os.Process.myPid());
        }
    }

    //弹窗方法
    private void alertDialogue(){
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("暂停");
        alert.setNeutralButton("继续",this);
        alert.setNegativeButton("退出",this);
        alert.create().show();
    }

    //====================================================================

    //暂停按键触发事件
    @Override
    public void onClick(View uiView) {
        int id = uiView.getId();
        if (id==R.id.stopBtn){
            //游戏暂停运行
            GamePause();
        }
    }

    //游戏暂停运行方法
    private void GamePause(){
        //游戏暂停运行
        view.stop();
        //音乐暂停播放
        mp.pause();
        //调用弹窗方法
        alertDialogue();
    }

    @Override
    public void run() {
        try {
            while (true){
                //当分数不超过最大分数上限时
                if(view.getScore()<1690){
                    //若判断用户车辆的状态为已死
                    if (GameView.isDead==true){
                        //将当前获得的分数用结束活动Intent来传输
                        endIntent.putExtra("score",GameView.getScore());
                        //执行活动跳转
                        startActivity(endIntent);
                        break;
                    }
                }else {
                    //分数大于最大分数上限，进入通关活动
                    startActivity(winIntent);
                    break;
                }
            }
            //循环停止下来这个活动就要被杀死
            android.os.Process.killProcess(android.os.Process.myPid());
        }catch (Exception e){

        }
    }
}
