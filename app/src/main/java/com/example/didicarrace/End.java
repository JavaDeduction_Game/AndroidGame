package com.example.didicarrace;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringDef;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Funny_One on 2017/12/5.
 */

public class End extends Activity implements View.OnClickListener{
    private TextView scoreText;//声明分数变量
    private Button endConfirmBtn;//定义结束返回安妮
    private Intent backActivity;//返回活动
    private Intent scoreReceiver;//


    //建立活动
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.end);//获取结束画面
        initUI();
    }

    //实例化UI方法
    private void initUI(){
        scoreText = (TextView)findViewById(R.id.end_score);//初始化分数id
        endConfirmBtn =(Button)findViewById(R.id.end_confirm);//初始化结束按钮id
        endConfirmBtn.setOnClickListener(this);//监听是否点击
        backActivity = new Intent(this,MainCover.class);
        scoreReceiver = getIntent();//连接主界面
        scoreText.setText(String.valueOf(scoreReceiver.getIntExtra("score",0)));//返回得分情况
    }



    //点击返回开始界面
    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.end_confirm){
            startActivity(backActivity);
            finish();
        }
    }
}
