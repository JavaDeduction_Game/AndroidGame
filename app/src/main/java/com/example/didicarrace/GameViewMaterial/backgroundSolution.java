package com.example.didicarrace.GameViewMaterial;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

import static com.example.didicarrace.GameView.S_height;
import static com.example.didicarrace.GameView.S_width;

/**
 * Created by Funny_One on 2017/11/4.
 */
//负责背景照片的处理
//代码规范人：20162317袁逸灏
public class backgroundSolution implements GameImage {
    //声明背景
    private Bitmap bg;
    //该bitmap充当背景的二级缓存
    private Bitmap newBitmap = null;
    //确定好图片下移的幅度
    private int heightLimit=0;

    public backgroundSolution(Bitmap bg){
        //背景实例化
        this.bg = bg;
        //二级缓存的实例化
        newBitmap = Bitmap.createBitmap(S_width, S_height,Bitmap.Config.ARGB_8888);
    }

    //将背景图片画到newBitmap中
    @Override
    public Bitmap getBitmap() {
        //实例化画笔
        Paint p = new Paint();
        Canvas canvas = new Canvas(newBitmap);

        //=========================画面下移的办法============================
        canvas.drawBitmap(bg,
                new Rect(0,0,bg.getWidth(),bg.getHeight()),
                new Rect(0,heightLimit,S_width,S_height+heightLimit)
                ,p);

        canvas.drawBitmap(bg
                , new Rect(0, 0, bg.getWidth(), bg.getHeight())
                , new Rect(0, -S_height+heightLimit,S_width, heightLimit)
                , p);
        //每次向下移15
        heightLimit+=10;
        //移够一个屏幕就归0，重新移动
        if(heightLimit >= S_height){
            heightLimit=0;
        }
        //====================================================================

        return newBitmap;
    }

    //========================代码重写的垃圾产物==========================
    @Override
    public int getX() {
        return 0;
    }

    @Override
    public int getY() {
        return 0;
    }
    //====================================================================

}
