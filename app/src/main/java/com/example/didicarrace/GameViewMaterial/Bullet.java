package com.example.didicarrace.GameViewMaterial;

import android.graphics.Bitmap;

import static com.example.didicarrace.GameView.bullets;

/**
 * Created by Funny_One on 2017/11/27.
 */
//子弹
//代码规范人：20162317袁逸灏
public class Bullet implements GameImage {
    //声明子弹
    private Bitmap bullet;
    //声明子弹的位置
    private int x,y;

    public Bullet(UserCar myCar,Bitmap bullet){
        //实例化子弹
        this.bullet=bullet;
        //===========子弹初始位置实例化=============
        x=(myCar.getX()+myCar.getCarWidth()/2)-8;
        y=myCar.getY()-bullet.getHeight();
        //==========================================
    }

    //图片获取方法
    @Override
    public Bitmap getBitmap() {
        //子弹的移动
        y-=50;
        if(y<-10){
            //出界后要移除当前对象
            bullets.remove(this);
        }
        return bullet;
    }

    //获取位置的横坐标
    @Override
    public int getX() {
        return x;
    }

    //获取位置的纵坐标
    @Override
    public int getY() {
        return y;
    }
}
