package com.example.didicarrace.GameViewMaterial;

import android.graphics.Bitmap;
import android.util.Log;

import com.example.didicarrace.GameView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.example.didicarrace.GameView.S_height;
import static com.example.didicarrace.GameView.S_width;
import static com.example.didicarrace.GameView.gameImage;

//敌人类
//代码规范人：20162317袁逸灏
public class Enemy implements GameImage{
    //获取分数
    private int score = GameView.getScore();
    //声明敌人
    public Bitmap diren = null;
    //敌车的车速
    public static int enemySpeed=5;
    //敌人车速的设置
    public static void setEnemySpeed(int speed){
        enemySpeed = speed;
    }
    //用于承载敌人的列表
    public List<Bitmap> bitmaps = new ArrayList<Bitmap>();
    //用于承载爆炸效果的列表
    public List<Bitmap> baozhas = new ArrayList<>();
    //声明位置
    public int x,y;
    //声明宽高
    public int width,height;
    //随机数
    private Random ran;
    //声明选取画面的索引
    public  int index =0;
    //画面切换的间隔
    public int num =0;
    //敌机所能承受的攻击
    public int sufferNum =0;
    //判断是否被毁灭了
    public boolean destroyed = false;
    //判断是否已经出界
    public  boolean isOut;


    public Enemy(Bitmap diren,Bitmap baozha){

        this.diren = diren;
        //从（0，0）开始截取diren.getWidth()/4那么宽,obstacle.getHeight()那么长的图。
        bitmaps.add(Bitmap.createBitmap(diren,0,0,diren.getWidth()/4,diren.getHeight()));

        bitmaps.add(Bitmap.createBitmap(diren,(diren.getWidth()/4)*1,0,diren.getWidth()/4,diren.getHeight()));
        bitmaps.add(Bitmap.createBitmap(diren,(diren.getWidth()/4)*2,0,diren.getWidth()/4,diren.getHeight()));
        bitmaps.add(Bitmap.createBitmap(diren,(diren.getWidth()/4)*3,0,diren.getWidth()/4,diren.getHeight()));

        //上面四个爆炸效果
        baozhas.add(Bitmap.createBitmap(baozha,0,0,baozha.getWidth()/4,baozha.getHeight()/2));
        baozhas.add(Bitmap.createBitmap(baozha,baozha.getWidth()/4,0,baozha.getWidth()/4,baozha.getHeight()/2));
        baozhas.add(Bitmap.createBitmap(baozha,(baozha.getWidth()/4)*2,0,baozha.getWidth()/4,baozha.getHeight()/2));
        baozhas.add(Bitmap.createBitmap(baozha,(baozha.getWidth()/4)*3,0,baozha.getWidth()/4,baozha.getHeight()/2));
        //下面四个爆炸效果
        baozhas.add(Bitmap.createBitmap(baozha,0,baozha.getHeight()/2,baozha.getWidth()/4,baozha.getHeight()/2));
        baozhas.add(Bitmap.createBitmap(baozha,(baozha.getWidth()/4)*1,baozha.getHeight()/2,baozha.getWidth()/4,baozha.getHeight()/2));
        baozhas.add(Bitmap.createBitmap(baozha,(baozha.getWidth()/4)*2,baozha.getHeight()/2,baozha.getWidth()/4,baozha.getHeight()/2));
        baozhas.add(Bitmap.createBitmap(baozha,(baozha.getWidth()/4)*3,baozha.getHeight()/2,baozha.getWidth()/4,baozha.getHeight()/2));


        //敌人飞机的宽和高
        width = diren.getWidth() /4;
        height = diren.getHeight();
        //敌机的起始位置设为屏幕外，能起到敌机从上方袭来的效果
        y = -diren.getHeight();

        ran = new Random();
        //减去（obstacle.getWidth()/4）是为了保证能够完整地显示出一台敌机

        x = ran.nextInt(S_width -(diren.getWidth() / 4) );
       while (x<152||x>750){
           x = ran.nextInt(S_width -(diren.getWidth() / 4) );
       }
        Log.i("tag", String.valueOf(x));

    }


    @Override
    public Bitmap getBitmap() {
        isOut = false;
        Bitmap bitmap = bitmaps.get(index);
        // num=7 地意义是让处于bitmap中的四张敌机状态图以一定的间隔来显示，从而产生动画效果
        if(num == 7){
            index++;
            //出界或被毁灭都要移除当前敌机对象
            if(index==8&&destroyed==true){
                gameImage.remove(this);


            }
            if(index == bitmaps.size()){
                index = 0;
            }
            num =0;
        }
        y+=enemySpeed;
        num++;
        //若飞机出界了
        if(y> S_height){
            isOut = true;
            gameImage.remove(this);
        }
        return bitmap;
    }

    //判断是否被攻击
    public void attacked(ArrayList<Bullet> bullets){

        if(sufferNum!=3) {
            if (!destroyed) {
                for (GameImage image : (List<GameImage>) bullets.clone()) {
                    if (image.getX() > x && image.getY() > y && image.getX() < x + width && image.getY() < y + height) {//判定子弹打中敌机
                        bullets.remove(image);//子弹打中敌机后消失，增强效果
                        sufferNum++;
                        break;
                    }
                }
            }
        }else {

            destroyed = true;
            bitmaps = baozhas;//将敌机中的图片替换为爆炸效果的图片

            //打破一架加20分
            score = score +20;
            //将新的总分传回游戏主界面中
            GameView.setScore(score);


            sufferNum =0;
        }
    }

    //====================获取位置的方法=====================
    @Override
    public int getX() {
        return x;
    }

    @Override
    public int getY() {
        return y;
    }
    //=======================================================

    //====================获取宽高的方法======================
    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
    //========================================================

}

