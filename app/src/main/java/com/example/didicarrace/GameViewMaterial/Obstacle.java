package com.example.didicarrace.GameViewMaterial;

import android.graphics.Bitmap;

import com.example.didicarrace.GameView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.example.didicarrace.GameView.S_height;
import static com.example.didicarrace.GameView.S_width;
import static com.example.didicarrace.GameView.gameImage;

/**
 * Created by user on 2017/11/29.
 */
//障碍物类
//代码规范人：20162317袁逸灏
public class Obstacle implements GameImage {
    //声明障碍物
    public Bitmap obstacle = null;
    //初始化障碍物移动速度
    public int obstacleSpeed =5;
    //判断障碍物是否已出界
    public  boolean isOut;
    //承载障碍物的列表
    public List<Bitmap> bitmaps = new ArrayList<Bitmap>();
    //声明障碍物的位置
    public int x,y;
    //声明障碍物的大小
    public int width,height;
    //声明选取画面的索引
    public  int index =0;
    //画面切换的间隔
    public int num =0;
    //随机数
    private Random ran;
    //==============鸡肋变量===============
    public int sufferNum =0;
    public boolean destroyed = false;
    //=====================================

    //障碍物的构造方法
    public Obstacle(Bitmap obstacle){
        this.obstacle = obstacle;
        bitmaps.add(Bitmap.createBitmap(obstacle,0,0,obstacle.getWidth()/4,obstacle.getHeight()));
        bitmaps.add(Bitmap.createBitmap(obstacle,(obstacle.getWidth()/4)*1,0,obstacle.getWidth()/4,obstacle.getHeight()));
        bitmaps.add(Bitmap.createBitmap(obstacle,(obstacle.getWidth()/4)*2,0,obstacle.getWidth()/4,obstacle.getHeight()));
        bitmaps.add(Bitmap.createBitmap(obstacle,(obstacle.getWidth()/4)*3,0,obstacle.getWidth()/4,obstacle.getHeight()));



        //障碍物的宽和高
        width = obstacle.getWidth() /4;
        height = obstacle.getHeight();

        //定义障碍物的初始位置是在屏幕外
        y = -obstacle.getHeight();

        ran = new Random();
        x = ran.nextInt(S_width -(obstacle.getWidth() / 4) );
        while (x<152||x>750){
            x = ran.nextInt(S_width -(obstacle.getWidth() / 4) );
        }
    }



    @Override
    public Bitmap getBitmap() {
        isOut = false;
        Bitmap bitmap = bitmaps.get(index);
        if(num == 7){
            index++;
            if(index==8&&destroyed==true){
                gameImage.remove(this);

            }

            if(index == bitmaps.size()){
                index = 0;
            }
            num =0;

        }
        y+= obstacleSpeed;
        num++;
        if(y> S_height){
            isOut = true;
            int score1 = GameView.getScore();
            //每移除一个...
            gameImage.remove(this);
            //分数加10
            score1 = score1 +10;
            //将新的总分传输到游戏界面中
            GameView.setScore(score1);
        }
        return bitmap;
    }



    //攻击判定
    public void attacked(ArrayList<Bullet> bullets){
        if(sufferNum!=100) {//设置障碍物吸收子弹的数量
            if (!destroyed) {
                for (GameImage image : (List<GameImage>) bullets.clone()) {
                    if (image.getX() > x && image.getY() > y && image.getX() < x + width && image.getY() < y + height) {//判定子弹打中敌机
                        bullets.remove(image);//子弹打中障碍物后消失，增强效果
                        sufferNum++;
                        break;
                    }
                }

            }

        }else {
            destroyed = true;

            sufferNum =0;
        }


    }
    //=====================获取障碍物的位置=======================
    @Override
    public int getX() {
        return x;
    }

    @Override
    public int getY() {
        return y;
    }
    //============================================================

    //=====================获取障碍物的大小=======================
    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
    //============================================================
}


