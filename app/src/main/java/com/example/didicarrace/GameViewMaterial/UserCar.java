package com.example.didicarrace.GameViewMaterial;

import android.graphics.Bitmap;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import static com.example.didicarrace.GameView.S_height;
import static com.example.didicarrace.GameView.S_width;
import static com.example.didicarrace.GameView.gameImage;

/**
 * Created by Funny_One on 2017/11/14.
 */
//用户使用的车辆
public class UserCar implements GameImage{
    private Bitmap usrCar;
    private int x;//声明高宽坐标
    private int y;//声明高宽坐标
    private int user_width,user_height;//声明用户车辆的宽度高度
    private List<Bitmap> bitmaps = new ArrayList<>();

    //===============测试区===================
    private boolean hit = false;
    //========================================


    public int getCarWidth(){
        return user_width;
    }//获取用户车辆宽度

    public int getCarHeight(){
        return  user_height;
    }//    获取用户车辆高度

    public UserCar(Bitmap usrCar){
        this.usrCar = usrCar;

        //截取用户车图片，每次选取1/4宽度的图片
        bitmaps.add(Bitmap.createBitmap(usrCar,0,0,usrCar.getWidth()/4,usrCar.getHeight()));
        bitmaps.add(Bitmap.createBitmap(usrCar,(usrCar.getWidth()/4)*1,0,usrCar.getWidth()/4,usrCar.getHeight()));
        bitmaps.add(Bitmap.createBitmap(usrCar,(usrCar.getWidth()/4)*2,0,usrCar.getWidth()/4,usrCar.getHeight()));
        bitmaps.add(Bitmap.createBitmap(usrCar,(usrCar.getWidth()/4)*3,0,usrCar.getWidth()/4,usrCar.getHeight()));

        //得到用户车辆的高和宽
        user_width = usrCar.getWidth()/4;
        user_height= usrCar.getHeight();

        //定义车辆在画面中的初始位置
        x=(S_width-user_width)/2;
        y =S_height-user_height-30;

    }


    public  int index =0;
    public int num =0;
    @Override
    public Bitmap getBitmap() {
        Bitmap bitmap = bitmaps.get(index);

        if(num == 7){ // num=7 地意义是让处于bitmap中的四张敌机状态图以一定的间隔来显示，从而产生动画效果
            index++;
            if(index==8){
                gameImage.remove(this);
            }
            if(index == bitmaps.size()){
                index = 0;
            }
            num =0;
        }
        num++;
        return bitmap;
    }


    //=========================测试区===============================
    //判断与敌机碰撞碰撞
    public boolean isCollsionWithE(Enemy en){
        Log.i("tag","=====================================");
       Log.i("tag", "usrWidth: "+String.valueOf(getCarWidth()));
        Log.i("tag","usrHeight: "+String.valueOf(getCarHeight()));


        if (en.destroyed==false&&en.isOut==false) {
            int ex = en.getX();
            int ey = en.getY();
            int ew = en.getWidth();
            int eh = en.getHeight();


            if (getX() >= ex && getX() >= ex + ew) {
                return false;
            } else if (getX() <= ex && getX() + getCarWidth() <= ex) {
                return false;
            } else if (getY() >= ey && getY() >= ey + eh) {
                return false;
            } else if (getY() <= ey && getY() + getCarHeight() <= ey) {
                return false;
            }
            hit = true;
            return true;
        }else {
            return false;
        }
    }

    //判断与障碍碰撞的方法
    public boolean isCollisionWithO(Obstacle ob){
        if (ob.isOut==false) {
            int ex = ob.getX();
            int ey = ob.getY();
            int ew = ob.getWidth();
            int eh = ob.getHeight();


            if (getX() >= ex && getX() >= ex + ew) {
                return false;
            } else if (getX() <= ex && getX() + getCarWidth() <= ex) {
                return false;
            } else if (getY() >= ey && getY() >= ey + eh) {
                return false;
            } else if (getY() <= ey && getY() + getCarHeight() <= ey) {
                return false;
            }
            hit = true;
            return true;
        }else {
            return false;
        }
    }
    //==============================================================

    @Override
    public int getX() {
        return x;
    }//获取横坐标的方法

    @Override
    public int getY() {
        return y;
    }//获取纵坐标的方法

    public void setY(int y){
        this.y = y;
    }//建立纵坐标

    public void setX(int x){
        this.x = x;
    }//建立横坐标
}
