package com.example.didicarrace.GameViewMaterial;

import android.graphics.Bitmap;

/**
 * Created by Funny_One on 2017/11/4.
 */

public interface GameImage {
    public Bitmap getBitmap();

        public int getX();

    public int getY();
}
