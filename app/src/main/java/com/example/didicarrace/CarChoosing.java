package com.example.didicarrace;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringDef;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.HashMap;

/**
 * Created by Funny_One on 2017/11/6.
 */
//选车界面
public class CarChoosing extends Activity implements View.OnTouchListener,View.OnClickListener{


    private EditText searching;//车辆搜索条
    private ImageView image_up1,image_up2,image_down1,image_down2;//放车的图片框
    private Intent StartGame; //活动跳转
    private Button confirmBtn;//查找按钮


    //建立活动
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carchoosing);//获取活动画面
        initUI();
    }

    //实例化UI组件
    private void initUI(){
        searching = (EditText)findViewById(R.id.choose_carSearching);
        confirmBtn = (Button)findViewById(R.id.choose_confirmBtn);
        confirmBtn.setOnClickListener(this);
        //=====================上界面车辆===========================
        //上面第一辆车初始化
        image_up1 = (ImageView)findViewById(R.id.choose_up_1);//定义UI组件名称
        image_up1.setImageResource(R.drawable.user_car_1);//选取第一辆车图片
        image_up1.setOnTouchListener(this);//监听是否点击

        //上面第二辆车初始化
        image_up2 = (ImageView)findViewById(R.id.choose_up_2);//定义UI组件名称
        image_up2.setImageResource(R.drawable.user_car_2);//选取第二辆车图片
        image_up2.setOnTouchListener(this);//监听是否点击

        //========================================================

        //======================下界面车辆==========================
        image_down1 = (ImageView)findViewById(R.id.choose_down_1);//定义UI组件名称
        image_down1.setImageResource(R.drawable.user_car_4);//选取第三辆车图片
        image_down1.setOnTouchListener(this);//监听是否点击



        image_down2 = (ImageView)findViewById(R.id.choose_down_2);//定义UI组件名称
        image_down2.setImageResource(R.drawable.user_car_3);//选取第四辆车图片
        image_down2.setOnTouchListener(this);//监听是否点击

        //========================================================
        StartGame = new Intent(this,MusicChoosing.class);//连接音乐界面
    }


    @Override//图片按钮方法
    public boolean onTouch(View view, MotionEvent motionEvent) {
        int id = view.getId();//获取UI组件名称
        if (id == R.id.choose_up_1){
            StartGame.putExtra("carID",String.valueOf(R.drawable.car1play));
            startActivity(StartGame);
            finish();

        }else if(id == R.id.choose_up_2){
            StartGame.putExtra("carID",String.valueOf(R.drawable.car2play));
            startActivity(StartGame);
            finish();
        }else if(id == R.id.choose_down_1){
            StartGame.putExtra("carID",String.valueOf(R.drawable.car4play));
            startActivity(StartGame);
            finish();
        }else if (id == R.id.choose_down_2){
            StartGame.putExtra("carID", String.valueOf(R.drawable.car3play));
            startActivity(StartGame);
            finish();
        }

        return false;
    }

    private int CarID=0;//声明车辆id

    //车辆查找方法
    private boolean Searching(String carName){
        boolean judge = false;
        HashMap hashMap = new HashMap();
        hashMap.put("Flash",R.drawable.car1play);
        hashMap.put("Lightening",R.drawable.car2play);
        hashMap.put("Flat",R.drawable.car4play);
        hashMap.put("Taxi",R.drawable.car3play);
        try {
            //哈希查找
            CarID= (int) hashMap.get(carName);
            judge= true;
        }catch (Exception e){
            Toast.makeText(this,"Can't find the car",Toast.LENGTH_SHORT).show();
        }
        return judge;
    }


    //查找方法按钮，转入音乐界面
    @Override
    public void onClick(View view) {
        if (view.getId()==R.id.choose_confirmBtn){
               if (Searching(searching.getText().toString())){
                   StartGame.putExtra("carID",String.valueOf(CarID));
                   startActivity(StartGame);
                   finish();

               }
        }
    }


}
