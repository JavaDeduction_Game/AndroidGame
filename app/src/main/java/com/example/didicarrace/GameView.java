package com.example.didicarrace;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.LinearLayout;

import com.example.didicarrace.GameViewMaterial.Bullet;
import com.example.didicarrace.GameViewMaterial.Enemy;
import com.example.didicarrace.GameViewMaterial.GameImage;
import com.example.didicarrace.GameViewMaterial.Obstacle;
import com.example.didicarrace.GameViewMaterial.UserCar;
import com.example.didicarrace.GameViewMaterial.backgroundSolution;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static android.R.attr.level;

/**
 * Created by Funny_One on 2017/11/4.
 */
//游戏画面
//代码规范者：20162317袁逸灏
public class GameView extends SurfaceView implements SurfaceHolder.Callback,Runnable,View.OnTouchListener{
    //判断用户车辆状态
    public static boolean isDead = false;
    //声明承载游戏元素类的列表
    public static ArrayList<GameImage> gameImage = new ArrayList();
    //声明继承子弹类的列表
    public static ArrayList<Bullet> bullets = new ArrayList<>();
    //声明自定义的surfaceholder（显示一个surface的抽象接口，使你可以控制surface的大小和格式， 以及在surface上编辑像素，和监视surace的改变。）
    private SurfaceHolder holder;
    //声明线程
    Thread thread = null;
    //-----------------------元素图片对象-----------------------
    private Bitmap usrCar;//用户车辆
    private Bitmap background;//背景
    private Bitmap bullet;//子弹
    private Bitmap explode;//爆炸效果;
    private Bitmap enemy1;//敌人1
    private Bitmap enemy2;//敌人2
    private Bitmap enemy3;//敌人3
    private Bitmap obstacle; //private Bitmap 障碍物
    //----------------------------------------------------------
    //声明用户车辆
    private UserCar myCar ;
    //声明用户车辆id
    private int carID;
    //声明敌人
    private Enemy e;
    //声明障碍物
    private Obstacle ob;
    private Bitmap erjihuancun;//二级缓存
    public static int S_width, S_height;//屏幕的宽高
    //============游戏线程状态===============
    private boolean state = true;
    private boolean stopState = false;
    //=======================================
    //===============关卡分数变量===================
    //初始化分数
    private  static int score=0;
    //--------------------分数的获得与设置--------------------------
    public static void setScore(int score1){
        score = score1;
    }
    public  static int getScore(){
        return score;
    }
    //--------------------------------------------------------------
    //初始化关卡
    private int level =1;
    //初始化敌人刷新频率
    private int enemyLevel =65;
    //初始化敌人速度
    private int enemySpeed=6;
    //关卡以及相关参数的二维数组
    private int [][] levelUp = {
            {1,150,65,5},
            {2,250,65,7},
            {3,375,65,9},
            {4,525,60,11},
            {5,700,50,13},
            {6,900,40,15},
            {7,1125,30,17},
            {8,1375,20,19},
            {9,1650,15,20},
    };
    //===============================================


        //自定义View的构造方法
        public GameView(Context context, AttributeSet attrs) {
            super(context,attrs);
            //获取反馈
            getHolder().addCallback(this);
            //触屏事件注册
            this.setOnTouchListener( this);


    }

    //堆图片进行初始化的处理方法
    private  void PictureSolution(){
        //-------------------------------加载图片-------------------------------------
        //加载用户车辆的图片
        usrCar = BitmapFactory.decodeResource(getResources(), carID);
        //加载背景图片
        background = BitmapFactory.decodeResource(getResources(),R.drawable.road);
        //获取子弹图片
        bullet =BitmapFactory.decodeResource(getResources(),R.drawable.bullet);
        //获取爆炸效果
        explode = BitmapFactory.decodeResource(getResources(), R.drawable.baozha);
        //。。。。。。。。。。。。。。。。获取敌人与障碍物。。。。。。。。。。。。。。。。。
        enemy1 = BitmapFactory.decodeResource(getResources(), R.drawable.enemy1);
        enemy2 = BitmapFactory.decodeResource(getResources(), R.drawable.enemy2);
        enemy3 = BitmapFactory.decodeResource(getResources(), R.drawable.enemy3); //敌人
        obstacle = BitmapFactory.decodeResource(getResources(), R.drawable.obstacle); //障碍物
        //。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。。
        //----------------------------------------------------------------------------

        //生成二级缓存照片
        erjihuancun = Bitmap.createBitmap(S_width,S_height,Bitmap.Config.ARGB_8888);
        //初始化用户车辆
        myCar = new UserCar(usrCar);
        //初始化敌人
        e = new Enemy(enemy1, explode);
        //初始化障碍物
        ob = new Obstacle(obstacle);

        //---------------------加载图片-----------------------
        gameImage.add(new backgroundSolution(background));
        gameImage.add(myCar);
        gameImage.add(e);
        gameImage.add(ob);
        //----------------------------------------------------
    }

    //获得选车时传入的ID
    public void carIDSetter(int carID){
        this.carID = carID;
    }

    //游戏线程停止的方法
    public void stop(){
        stopState =true;
    }
    //游戏线程启动的方法
    public void start(){
        stopState = false;
        thread.interrupt();//通知线程运行起来
    }


    @Override
    public void run() {
        //敌人刷新冷却
        int EnemyNum = 0;
        //子弹发出间隔
        int bullrtNum=0;
        //画游戏画面的画笔
        Paint p1  = new Paint();
        //============画分数和关卡信息的画笔============
        Paint p2 = new Paint();
        //设置画笔颜色
        p2.setColor(Color.YELLOW);
        //设置画笔写出字的大小
        p2.setTextSize(50);
        //设置防抖动为真，写出的字会变得柔和
        p2.setDither(true);
        //抗锯齿
        p2.setAntiAlias(true);
        //==============================================
        try{
            while (state){

                //当线程被暂停的时候
                while (stopState){
                    try {
                        Thread.sleep(1000000);
                    }catch (Exception e){

                    }
                }


                //创建二级画布
                Canvas newCanvas = new Canvas(erjihuancun);

                //敌人的处理
                for (GameImage image : (List<GameImage>)gameImage.clone()) {
                    if(image instanceof Enemy){
                        ((Enemy)image).attacked(bullets);//把子弹告诉敌机

                    }
                    //将整幅图画画在怎么画面上
                    newCanvas.drawBitmap(image.getBitmap(), image.getX(), image.getY(), p1);
                }

                //障碍物的处理
                for (GameImage image : (List<GameImage>)gameImage.clone()) {
                    if(image instanceof Obstacle){
                        ((Obstacle)image).attacked(bullets);//把子弹告诉障碍物
                    }
                    //将整幅图画画在怎么画面上
                    newCanvas.drawBitmap(image.getBitmap(), image.getX(), image.getY(), p1);
                }


                //对于整个游戏画面的处理
                for(GameImage image: (List<GameImage>)gameImage.clone()){
                    //将整幅图画画在怎么画面上
                    newCanvas.drawBitmap(image.getBitmap(),image.getX(),image.getY(),p1);
                }

                //子弹
                for (Bullet bt : (List<Bullet>)bullets.clone()){
                    newCanvas.drawBitmap(bt.getBitmap(),bt.getX(),bt.getY(),p1);
                }

                //将文字画在游戏画面中
                newCanvas.drawText("Score:"+score,0,50,p2);
                newCanvas.drawText("Level: "+level,0,100,p2);

                //升级系统
                if (levelUp[level - 1][1] <= score) {
                    enemyLevel = levelUp[level][2];
                    enemySpeed = levelUp[level][3];
                    level = levelUp[level][0];


                }

                //子弹刷新系统
                if(myCar!=null){
                    //每发子弹之间的频率为10
                    if (bullrtNum==10) {
                        bullrtNum=0;
                        bullets.add(new Bullet(myCar, bullet));
                    }
                    bullrtNum++;
                }

                //====================敌人生成系统====================
                if(EnemyNum == enemyLevel||EnemyNum>enemyLevel){
                    EnemyNum = 0;
                    Random enemychoice=new Random();
                    int a=enemychoice.nextInt(5);
                    if(a==0) {
                        e =new Enemy(enemy1, explode);
                        gameImage.add(e);
                    }if (a==1) {
                        e=new Enemy(enemy2, explode);
                        gameImage.add(e);
                    }if (a==2) {
                        e= new Enemy(enemy3, explode);
                        gameImage.add(e);

                    }if (a==4){
                        ob = new Obstacle(obstacle);
                        gameImage.add(ob);
                    }
                }
                EnemyNum++;
                //====================================================

                //====================游戏画面绘制入自定义View中======================
                Canvas  canvas = holder.lockCanvas();
                canvas.drawBitmap(erjihuancun,0,0,p1);
                holder.unlockCanvasAndPost(canvas);
                //====================================================================

                //用户车辆与敌方车辆相撞的判断
                if(myCar.isCollsionWithE(e)){
                    isDead = true;
                }
                //用户车辆与障碍物相撞的判断
                if (myCar.isCollisionWithO(ob)){
                   isDead = true;
                }

                Thread.sleep(0);
                //将敌人速度的数据传去敌人的类中
                Enemy.setEnemySpeed(enemySpeed);

            }


        }catch (Exception e){

        }

    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {

    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int width, int height) {
        this.S_width = width;
        this.S_height = height;
        PictureSolution();
        //实例化surfaceHolder
        this.holder = surfaceHolder;
        //线程状态该为运行
        state = true;
        //线程的实例化
        thread = new Thread(this);
        //开始运行线程
        thread.start();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        state = false;
    }




    //触碰车辆并进行拖拽而使车辆进行移动的功能
    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {

        try {
            //===================实现手指让用户车辆在屏幕中滑动=========================
            //用户点击屏幕的时候
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {

                for (GameImage game : gameImage) {
                    if (game instanceof UserCar) {
                        UserCar car = (UserCar) game;
                        if (car.getX() < motionEvent.getX()
                                && car.getY() < motionEvent.getY()
                                && car.getX() + car.getCarWidth() > motionEvent.getX()
                                && car.getY() + car.getCarHeight() > motionEvent.getY()
                                ) {
                            myCar = car;
                        } else {
                            myCar = null;
                        }
                        break;
                    }
                }
            }
            //用户滑动屏幕的时候
            else if (motionEvent.getAction() == motionEvent.ACTION_MOVE
                    ) {


                if (myCar != null && (int) motionEvent.getX() - myCar.getCarWidth() / 2 > 196 && (int) motionEvent.getX() - myCar.getCarWidth() / 2 < 760) {
                    myCar.setX((int) motionEvent.getX() - myCar.getCarWidth() / 2);
                    myCar.setY((int) motionEvent.getY() - myCar.getCarHeight() / 2);
                }
                //手离开屏幕的时候
                else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {

                    myCar = null;
                }

            }
            //==========================================================================

        }catch (Exception e){

        }
            return true;

    }




}
