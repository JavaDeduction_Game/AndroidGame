package com.example.didicarrace;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;


public class MainCover extends AppCompatActivity implements View.OnClickListener {//主界面类

    private Intent startIntent;    //该Intent连接游戏运行的Intent
    private Button btnStartL,btnQuit;//定义开始游戏和退出游戏的按钮

    //建立活动
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);//获取连接主活动界面
        initUI();
    }

    //进行UI组件初始化
    private void initUI(){
        startIntent = new Intent(this,CarChoosing.class);

        //闯关模式按键初始化
        btnStartL = (Button) findViewById(R.id.btnGameStartLevel);
        btnStartL.setOnClickListener(this);

        //退出游戏按键初始化
        btnQuit = (Button)findViewById(R.id.btnQuitGame);
        btnQuit.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        //闯关模式按键触发事件
        if(view.getId()==R.id.btnGameStartLevel){
            startActivity(startIntent);
            finish();
        }

        //退出游戏按键触发事件
        else if(view.getId()==R.id.btnQuitGame){

            android.os.Process.killProcess(android.os.Process.myPid());
        }
    }
}
