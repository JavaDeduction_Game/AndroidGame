package com.example.didicarrace;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Funny_One on 2017/12/4.
 */

public class Win extends Activity implements View.OnClickListener{
    //返回活动跳转
    private Intent backIntent;
    //接收分数
    private Intent scoreReceiver;
    //分数文本框
    private TextView scoreText;
    //返回主活动按键
    private Button confirmBtn;

    //建立活动
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.win);//获取获胜界面
        initUI();
    }


    //组件初始化方法
    private void initUI(){
        scoreReceiver = getIntent();
        backIntent = new Intent(this,MainCover.class);
        scoreText = (TextView)findViewById(R.id.win_score);
        confirmBtn = (Button)findViewById(R.id.win_confirm);
        confirmBtn.setOnClickListener(this);
//
    }

    @Override
    public void onClick(View view) {
        if (view.getId()==R.id.win_confirm){
            startActivity(backIntent);//连接返回主界面
            finish();
        }
    }
}
