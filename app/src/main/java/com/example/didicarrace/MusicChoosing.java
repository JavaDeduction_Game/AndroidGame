package com.example.didicarrace;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.didicarrace.GameViewMaterial.Bullet;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Funny_One on 2017/11/27.
 */
//歌单
public class MusicChoosing extends Activity implements View.OnClickListener {
    private ListView listView;
    private Intent idReceiver;//获取车辆ID
    private Intent StartGame;//开始游戏的活动跳转
    private Button searchBtn; //确认按键
    private EditText findSong; //歌曲查找框
    private String[] songName = {"Sat it ain't so","Fahrenheit","在你身边","Fly Away","Kick Ass"};//歌曲名称文本
    private int[] songID = {R.raw.bgm1,R.raw.bgm2,R.raw.bgm3,R.raw.bgm4,R.raw.bgm5};//歌曲连接名称

    //建立活动
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.music_selection);//获取选歌界面
        idReceiver=getIntent();
        initUI();
    }

    //组件初始化
    private void initUI(){
        //歌曲查找按键初始化
        searchBtn = (Button)findViewById(R.id.music_searchBtn);
        searchBtn.setOnClickListener(this);

        StartGame = new Intent(this,LevelProcedure.class);

        //连接音乐列表的适配器
        MusicListAdapter adapter = new MusicListAdapter(this,songName);
        findSong = (EditText)findViewById(R.id.music_find);
        listView = (ListView)findViewById(R.id.music_list);
        listView.setAdapter(adapter);

        //匹配音乐与用户车
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                StartGame.putExtra("songID",songID[i]);
                StartGame.putExtra("carID",idReceiver.getStringExtra("carID"));
                startActivity(StartGame);
                finish();
            }
        });
    }


    //歌曲查找事件的各种活动
    @Override
    public void onClick(View view) {
        if (view.getId()==R.id.music_searchBtn){
            String target = findSong.getText().toString();
            int resultIndex = Searching.linearSearch(songName,target);
            if (resultIndex!=-1){
                StartGame.putExtra("songID",songID[resultIndex]);
                StartGame.putExtra("carID",idReceiver.getStringExtra("carID"));
                startActivity(StartGame);
                finish();
            }else {
                Toast.makeText(this,"曲目不存在",Toast.LENGTH_SHORT).show();
            }
        }
    }

}
