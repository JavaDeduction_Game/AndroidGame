package com.example.didicarrace;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Created by Funny_One on 2017/11/27.
 */

//音乐列表的适配器
public class MusicListAdapter extends BaseAdapter {
    private Context context;//当前活动环境
    private View Myview;//适配器样式
    private String[] musicName ;//音乐名称

    //定义适配器方法
    public MusicListAdapter(Context context,String[] musicName){
        this.context = context;
        this.musicName = musicName;
    }
    @Override
    public int getCount() {
        return musicName.length;
    }//返回歌曲数量

    @Override
    public Object getItem(int i) {
        return musicName[i];
    }//返回歌曲名称

    @Override
    public long getItemId(int i) {
        return i;
    }//返回歌曲Id

    //获取适配器的样式
    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = LayoutInflater.from(context);
        Myview = inflater.inflate(R.layout.music_selection_list,null);
        TextView text = (TextView)Myview.findViewById(R.id.music_list_text);
        text.setText(musicName[position]);
        return Myview;
    }
}
