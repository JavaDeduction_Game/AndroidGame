package com.example.didicarrace;

import java.util.ConcurrentModificationException;

/**
 * Created by Funny_One on 2017/9/18.
 */

//查找类
public class Searching {

    //线性查找方法
    public static int  linearSearch(String[] data, String target){
        boolean judge = false;
        int index =0;
        while (index < data.length){
            if(data[index].equals(target)){
                judge = true;
                break;
            }
            index++;
        }
        if (!judge){
            index = -1;
        }
        return index;
    }


    //二分查找方法
    public static  Comparable binarySearch(Comparable[] data,Comparable target){
        Comparable result = null;
        int first =0, last = data.length-1,mid;

        while (result == null && first <= last){
            mid = (first + last) / 2;
            if(data[mid].compareTo(target)==0){
                result = data[mid];
            }else if(data[mid].compareTo(target)>0){
                last = mid -1;
            }else {
                first = mid + 1;
            }
        }
        return result;
    }


}
